Purpose
=======

Setun was the world's first ternary computer working on the base of -1, 0, 1 instead of the usual binary with the base 0, 1. To read more about that mashine, start here: http://en.wikipedia.org/wiki/Setun

This repository contains the scans of the original documentation: Brusentsov "Malaya vycheslitiljnaya mashina 'Setun'" (1965). Please be aware that the original is written in Russian!

How to download
===============

1. Go to doc and select a PDF file
2. The PDF file will open. You can read it online or download it.

Hint
====

The scans are in a raw format, hence the files are really big. Moreover, every 2nd page is wrong way round.